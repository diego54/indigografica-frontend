import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';
import 'hammerjs';
import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ProductComponent } from './component/product/product.component';
import { ProductCategoryComponent } from './component/product-category/product-category.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
MatAutocompleteModule,
MatButtonModule,
MatButtonToggleModule,
MatCardModule,
MatCheckboxModule,
MatChipsModule,
MatDatepickerModule,
MatDialogModule,
MatDividerModule,
MatExpansionModule,
MatGridListModule,
MatIconModule,
MatInputModule,
MatListModule,
MatMenuModule,
MatNativeDateModule,
MatPaginatorModule,
MatProgressBarModule,
MatProgressSpinnerModule,
MatRadioModule,
MatRippleModule,
MatSelectModule,
MatSidenavModule,
MatSliderModule,
MatSlideToggleModule,
MatSnackBarModule,
MatSortModule,
MatStepperModule,
MatTableModule,
MatTabsModule,
MatToolbarModule,
MatTooltipModule,
  MAT_DATE_LOCALE
} from '@angular/material';
import { HomeComponent } from './home/home.component';
import { SidebarNavigationComponent } from './component/sidebar-navigation/sidebar-navigation.component';
import { CartComponent } from './component/cart/cart.component';
import {ProductOperationService} from './service/product-operation.service';
import { PersonalDataDialogComponent } from './component/personal-data-dialog/personal-data-dialog.component';
import { DesignPreviewComponent } from './component/design-preview/design-preview.component';


@NgModule({
  declarations: [
    AppComponent,
    ProductCategoryComponent,
    ProductComponent,
    HomeComponent,
    SidebarNavigationComponent,
    CartComponent,
    PersonalDataDialogComponent,
    DesignPreviewComponent
  ],
  entryComponents: [
    PersonalDataDialogComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatStepperModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule
  ],
  providers: [{provide: LOCALE_ID, useValue: 'es-AR'},
    {provide: MAT_DATE_LOCALE, useValue: 'es-AR'},
    ProductOperationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
